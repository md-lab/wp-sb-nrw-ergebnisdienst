<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:template match="tabelle">
      <xsl:comment>SBNRW-Plugin by media design lab http://md-lab.de</xsl:comment>
       <h3>Tabelle: <xsl:apply-templates select="tname" /></h3>
        <table border="1" class="nrwportalKreuzTabelle">
        <thead>
          <tr>
            <th><abbr title="Platz">Pl.</abbr></th>
            <th>Mannschaft</th>
            <xsl:apply-templates select="kreuzHeader" />
            <th><abbr title="Anzahl Spiele">Sp.</abbr></th>
            <th><abbr title="Mannschaftspunkte">MP</abbr></th>
            <th><abbr title="Brettpunkte">BP</abbr></th>
          </tr>
        </thead>
        <xsl:apply-templates select="rangliste" />
        </table>
    </xsl:template>

   <xsl:template match="rangliste">
        <tbody>
        <xsl:apply-templates select="teams" />
        </tbody>
    </xsl:template>

    <xsl:template match="teams">
        <tr>
          <td><xsl:value-of select="platz" /></td>
          <td><xsl:value-of select="team" /></td>
          <xsl:apply-templates select="kreuzBody" />
          <td><xsl:value-of select="spiele" /></td>
          <td><xsl:value-of select="mp" /></td>
          <td><xsl:value-of select="bp" /></td>
        </tr>
    </xsl:template>

    <xsl:template match="kreuzHeader">
        <xsl:apply-templates select="eH" />
    </xsl:template>
    
    <xsl:template match="kreuzBody">
        	<xsl:apply-templates select="e" />
        	
    </xsl:template>
    
    <xsl:template match="e">
			<xsl:if test="eRR"><td><xsl:value-of select="eHR" /><xsl:apply-templates select="eRR" /></td></xsl:if>
			<xsl:if test="not(eRR)"><td><xsl:value-of select="." /></td></xsl:if>
    </xsl:template>
    
    <xsl:template match="eH">
    	<th><xsl:value-of select="." /></th>
    </xsl:template>
    
    <xsl:template match="eHR">
    	<xsl:value-of select="." />
    </xsl:template>
    
    <xsl:template match="eRR">
    	<br /><xsl:value-of select="." />
    </xsl:template>

</xsl:stylesheet>