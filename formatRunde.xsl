<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:template match="einzelergebnisse">
	<xsl:apply-templates select="spieltag" />
	</xsl:template>
		<xsl:template match="begegnung">
                <xsl:comment>SBNRW-Plugin by media design lab http://md-lab.de</xsl:comment> 
				<table border="1" class="nrwportalRunde">
				<tr>
				  <th>Br.</th>
				  <th>Rangnr.</th>
				  <th><xsl:apply-templates select="heimmannschaft" /></th>
				  <th>-</th>
				  <th>Rangnr.</th>
				  <th><xsl:apply-templates select="gastmannschaft" /></th>
				  <th><xsl:apply-templates select="ergebnis" /></th>
				</tr>
        		<xsl:apply-templates select="einzelergebnis" />
        	</table>
    
    </xsl:template>

    <xsl:template match="einzelergebnis">
    	<tr>
        	<td><xsl:apply-templates select="brettnr" /></td>
        	<td><xsl:apply-templates select="RangnrHeim" /></td>
            <td><xsl:apply-templates select="heimspieler" /></td>
            <td>-</td>
            <td><xsl:apply-templates select="RangnrGast" /></td>
            <td><xsl:apply-templates select="gastspieler" /></td>
            <td><xsl:apply-templates select="ergebnis" /></td>
        </tr>
    </xsl:template>

<!--	<xsl:template match="brettnr">
        <td><xsl:apply-templates select="." />####</td>
    </xsl:template>-->
    
    <xsl:template match="heimspieler">
        <xsl:apply-templates select="nachname" />, <xsl:apply-templates select="vorname" />
    </xsl:template>

    <xsl:template match="ergebnis">
        <xsl:apply-templates select="heimergebnis" />
		:
		<xsl:apply-templates select="gastergebnis" />
    </xsl:template>

    <xsl:template match="gastspieler">
		<xsl:apply-templates select="nachname" />, <xsl:apply-templates select="vorname" />
    </xsl:template>

    <xsl:template match="vorname">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="nachname">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="heimmannschaft">
        <xsl:apply-templates select="name" />
        <xsl:apply-templates select="nr" />
    </xsl:template>
    <xsl:template match="gastmannschaft">
        <xsl:apply-templates select="name" />
        <xsl:apply-templates select="nr" />
    </xsl:template>

    <xsl:template match="name">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="nr">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="runde">
        <xsl:value-of select="." />
    </xsl:template>

<!--	<xsl:template match="brettnr">
        <xsl:value-of select="." />
    </xsl:template>-->

    <xsl:template match="spieltag">
        <h3>Runde 
        <xsl:apply-templates select="runde" />
        am
        <xsl:apply-templates select="datum" />
        um
        <xsl:apply-templates select="uhrzeit" />
        Uhr</h3>
    </xsl:template>

	<xsl:template match="runde">
        <xsl:value-of select="." />
    </xsl:template>
    
    <xsl:template match="datum">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="uhrzeit">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="heimmannschaft">
        <xsl:apply-templates select="name" />
        <xsl:text>&#x00A0;</xsl:text>
        <xsl:apply-templates select="nr" />
    </xsl:template>

    <xsl:template match="gastmannschaft">
        <xsl:apply-templates select="name" />
        <xsl:text>&#x00A0;</xsl:text>
        <xsl:apply-templates select="nr" />
    </xsl:template>

</xsl:stylesheet>