<?php

/**
 * Main Plugin File
 *
 * @package    SB-NRW
 * @author     S. Grinschgl <info@md-lab.de>
 * @copyright  Copyright (c) 2016, Media-Design-Lab - Sascha Grinschgl
 * @license    https://en.wikipedia.org/wiki/Donationware
 * @link       http://md-lab.de/sb-nrw-plugin
 *
 * Plugin Name: Schachbund NRW Ergebnisdienst
 * Plugin URI: http://wordpress.org/plugins/SB-NRW/
 * Description: Shortcodes: [sbnrw-tabelle id="1397"], [sbnrw-kreuztabelle id="1397"] und [sbnrw-runde id="1397" runde="1" begegnung="3"]
 * Version: 1.0.0
 * Author: S. Grinschgl
 * Author URI: https://md-lab.de
 * License: GPLv2
 **/

/**
 * @package SB-NRW
 * @version 1.0
 */

	// timeout of 5 seconds, if API is not available
	$context = stream_context_create(array('http'=> array(
		'timeout' => 5.0,
		'ignore_errors' => true,
	)));

	libxml_set_streams_context($context);

	defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
	
	// Register style sheet.
	add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );

	/**
	 * Register style sheet.
	 */
	function register_plugin_styles() {
		wp_register_style( 'sb-nrw', plugins_url( 'SB-NRW/sb-nrw.css' ) );
		wp_enqueue_style( 'sb-nrw' );
	}

function tabelle_func($atts){

//return "foo = {$atts['foo']}";

			// Einstellungen:
			// Turnier-ID
			$tid = $atts['id'];

			// welches Turnier welche ID hat, können Sie aus der Adresse ablesen.
			// Ein Beispiel: Die Oberliga NRW 2010/11 ist über
			// http://nrw.svw.info/ergebnisse/show/2010/10/
			// zu erreichen; die gesuchte ID steckt im letzten Teil der Adresse: 10

			// Transformationsanweisungen für die Darstellung der Tabelle
			$xslt  = plugin_dir_path( __FILE__ ).'formatTabelle.xsl';

			// Wenn Sie eine Kreuztabelle möchten, entfernen Sie die Kommentarzeichen in der
			// nächsten Zeile
			// $xslt    = 'formatTabelleKreuz.xsl';

			// laden Sie sich auch die entsprechende Transformationsdatei herunter (s.u)

			// weitere Einstellungen; müssen nicht geändert werden!

			// wir cachen die Daten für zwei Stunden; spart Resourcen
			$tstamp  = time()-7200;

			// Name einer temporären Datei
			$tmpfile = plugin_dir_path( __FILE__ ).'temp/tmp_tabelle_'.$tid.'.xml';

			// Name der Datei, in der die Daten lokal abgelegt werden
			$tabelle = plugin_dir_path( __FILE__ ).'tabellen/tabelle_'.$tid.'.xml';

			// die Daten-Quelle
			$xml     = 'http://nrw.svw.info/tools/export/tabelle.php?tid='.$tid;


			// Test, ob Datei vorhanden und nicht zu alt
			if ((!is_file($tabelle)) || (filectime($tabelle)<$tstamp)) {
				if (@copy($xml,$tmpfile, false, $context)) {
					// ist eine alte Tabelle vorhanden, dann löschen
					if (is_file($tabelle)) {
						unlink($tabelle);
					}
					// verschiebe temporäre Datei
					rename($tmpfile , $tabelle);
				}
				else {
					echo "<p>konnte Tabelle nicht auslesen!</p>";
				}
			}

			// Transformation: Lade Transfomationsanweisung
			$xslDoc1 = new DOMDocument();
			$xslDoc1->load($xslt);
			// Transformation: Lade Tabelle
			$xmlDoc = new DOMDocument();
			$xmlDoc->load($tabelle);
			// Transformation ins html
			$xsltproc = new XSLTProcessor();
			$xsltproc->importStylesheet($xslDoc1);

			// Ausgabe
			return $xsltproc->transformToXML($xmlDoc);
			// Liefern Sie Ihre Webseite nicht als UTF-8 kodiert aus, löschen
			// Sie die obige Zeile und verwenden Sie statt dessen den folgenden
			// Befehl: (einfach die "//" in der nächsten Zeile entfernen)
			//echo iconv('UTF-8','ISO-8859-1',$xsltproc->transformToXML($xmlDoc));
			
	};

	function kreuztabelle_func($atts){
			// Einstellungen:
			// Turnier-ID
			$tid = $atts['id'];

			// welches Turnier welche ID hat, können Sie aus der Adresse ablesen.
			// Ein Beispiel: Die Oberliga NRW 2010/11 ist über
			// http://nrw.svw.info/ergebnisse/show/2010/10/
			// zu erreichen; die gesuchte ID steckt im letzten Teil der Adresse: 10

			// Transformationsanweisungen für die Darstellung der Tabelle
			$xslt    = plugin_dir_path( __FILE__ ).'formatTabelleKreuz.xsl';

			// wir cachen die Daten für zwei Stunden; spart Resourcen
			$tstamp  = time()-7200;

			// Name einer temporären Datei
			$tmpfile = plugin_dir_path( __FILE__ ).'temp/tmp_tabelle_'.$tid.'.xml';

			// Name der Datei, in der die Daten lokal abgelegt werden
			$tabelle = plugin_dir_path( __FILE__ ).'tabellen/tabelle_'.$tid.'.xml';

			// die Daten-Quelle
			$xml     = 'http://nrw.svw.info/tools/export/tabelle.php?tid='.$tid;

			// Test, ob Datei vorhanden und nicht zu alt
			if ((!is_file($tabelle)) || (filectime($tabelle)<$tstamp)) {
				if (@copy($xml,$tmpfile, false, $context)) {
					// ist eine alte Tabelle vorhanden, dann löschen
					if (is_file($tabelle)) {
						unlink($tabelle);
					}
					// verschiebe temporäre Datei
					rename($tmpfile , $tabelle);
				}
				else {
					echo "<p>konnte Tabelle nicht auslesen!</p>";
				}
			}

			// Transformation: Lade Transfomationsanweisung
			$xslDoc1 = new DOMDocument();
			$xslDoc1->load($xslt);
			// Transformation: Lade Tabelle
			$xmlDoc = new DOMDocument();
			$xmlDoc->load($tabelle);
			// Transformation ins html
			$xsltproc = new XSLTProcessor();
			$xsltproc->importStylesheet($xslDoc1);

			// Ausgabe
			return $xsltproc->transformToXML($xmlDoc);
			// Liefern Sie Ihre Webseite nicht als UTF-8 kodiert aus, löschen
			// Sie die obige Zeile und verwenden Sie statt dessen den folgenden
			// Befehl: (einfach die "//" in der nächsten Zeile entfernen)
			//echo iconv('UTF-8','ISO-8859-1',$xsltproc->transformToXML($xmlDoc));
			
	};

	function rundenergebnis_func($atts){
		// Einstellungen:
		// Turnier-ID
		$tid = $atts['id'];

		// Rundennummer
		$runde = $atts['runde'];

		// Begegnung an diesem Spieltag:
		$begegnung = $atts['begegnung'];

		// wenn eine bestimmte Begegnung angezeigt werden soll, geben Sie diese hier an;
		// Oben beginnend wird von 1 an weiter gezählt; Ein Beispiel:
		// http://nrw.svw.info/ergebnisse/show/2010/10/runde/3/
		// Die Begegnung SV Castrop-Rauxel 1 - PSV Duisburg 1 ist somit
		// Begegnung Nr. 4 in der 3. Runde mit der Turnier-ID 10

		// sollen alle Spiele angezeigt werden, setzen Sie oben $begegnung = 0;

		// weitere Einstellungen; sollen nicht geändert werden!
		// wir cachen die Daten für zwei Stunden; spart Resourcen
		$tstamp  = time()-7200;
		// Name einer temporären Datei
		$tmpfile = plugin_dir_path( __FILE__ )."temp/tmp_".$tid."_".$runde."_".$begegnung.".xml";
		// Name der Datei, in der die Daten lokal abgelegt werden
		$tabelle = plugin_dir_path( __FILE__ )."runden/runde_".$tid."_".$runde."_".$begegnung.".xml";
		// Transformationsanweisungen für die Darstellung der Ergebnisse
		$xslt    = plugin_dir_path( __FILE__ ).'formatRunde.xsl';
		// die Daten-Quelle
		$xml     = "http://nrw.svw.info/tools/export/runde.php?tid=$tid&runde=$runde&begegnung=$begegnung";


		// Test, ob Datei vorhanden und nicht zu alt
		if ((!is_file($tabelle)) || (filectime($tabelle)<$tstamp)) {
			if (@copy($xml,$tmpfile, false, $context)) {
				// ist eine alte Datei vorhanden, dann löschen
				if (is_file($tabelle)) {
					unlink($tabelle);
				}
				// verschiebe temporäre Datei
				rename($tmpfile , $tabelle);
			}
			else {
				echo "<p>konnte Runde nicht auslesen!</p>";
			}
		}

		// Transformation: Lade Transfomationsanweisung
		$xslDoc1 = new DOMDocument();
		$xslDoc1->load($xslt);
		// Transformation: Lade Tabelle
		$xmlDoc = new DOMDocument();
		$xmlDoc->load($tabelle);
		// Transformation ins html
		$xsltproc = new XSLTProcessor();
		$xsltproc->importStylesheet($xslDoc1);

		// um eine in allen Fällen richtige Anzeige des 1/2-Zeichens zu gewährleisten
		// ist dieses Zeichen (sofern vorhanden) maskiert. Diese Maskierung machen
		// wir hier rückgängig
		$string = str_replace("&amp;" , "&" , $xsltproc->transformToXML($xmlDoc));

		// Ausgabe
		return $string;
		// Liefern Sie Ihre Webseite nicht als UTF-8 kodiert aus, löschen
		// Sie die obige Zeile und verwenden Sie statt dessen den folgenden
		// Befehl: (einfach die "//" in der nächsten Zeile entfernen)
		// echo iconv('UTF-8','ISO-8859-1',$string);
		
	};
	add_shortcode( 'sbnrw-tabelle', 'tabelle_func' );
	add_shortcode( 'sbnrw-kreuztabelle', 'kreuztabelle_func' );
	add_shortcode( 'sbnrw-runde', 'rundenergebnis_func' );

?>