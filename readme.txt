=== Schachbund NRW Ergebnisdienst ===
Contributors: mdlab
Tags: Schach, Chess, Schachbund, API
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CKS8D5YW7M2FQ&source=url
Requires at least: 3.8
Tested up to: 5.3
License: GPLv2
License URI: https://www.gnu.de/documents/gpl-2.0.de.html

Schachbund NRW - Ergebnisdienst Plugin

Implementierung der SB-NRW-Ergebnisdienst-Api als WordPress-Plugin via Shortcode.


== Description ==
Schachbund NRW Ergebnisdienst
Shortcodes: 
Tabelle, z.B.:
[sbnrw-tabelle id=\"1397\"] und [sbnrw-kreuztabelle id=\"1397\"] 
Rundenergebnis, z.B.:
 [sbnrw-runde id=\"1397\" runde=\"1\" begegnung=\"3\"]
(begegnung=\"0\" listet alle Begegnungen des Spieltages)


== Installation ==

Installation:

Manuell:
    Upload des Verzeichnisses SB-NRW in das /plugin Verzeichnis
    Aktiviere das Plugin mittels \'Plugins\'-Menü in WordPress
    Fertig!

Shortcodes s. Beschreibung



== Changelog ==
v 1.0.0 initial release